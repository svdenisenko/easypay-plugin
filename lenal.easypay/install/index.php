<?php

IncludeModuleLangFile(__FILE__);

class lenal_easypay extends CModule
{
    public $MODULE_ID = 'lenal.easypay';
    public $MODULE_GROUP_RIGHTS = 'N';

    public $PARTNER_NAME = 'www.easypay.ua';
    public $PARTNER_URI = 'https://www.easypay.ua';

    public function __construct()
    {
        require(dirname(__FILE__).'/version.php');

        $this->MODULE_NAME = GetMessage('EP_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('EP_MODULE_DESC');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
    }

    public function DoInstall()
    {
        if (IsModuleInstalled('sale')) {
            global $APPLICATION;
            $this->InstallFiles();
            RegisterModule($this->MODULE_ID);
            return true;
        }

        $MODULE_ID = $this->MODULE_ID;
        $TAG = 'EPAY';
        $MESSAGE = GetMessage('EP_ERR_MODULE_NOT_FOUND', array('#MODULE#'=>'sale'));
        $intID = CAdminNotify::Add(compact('MODULE_ID', 'TAG', 'MESSAGE'));

        return false;
    }

    public function DoUninstall()
    {
        global $APPLICATION;
        COption::RemoveOption($this->MODULE_ID);
        UnRegisterModule($this->MODULE_ID);
        $this->UnInstallFiles();
    }

    public function InstallFiles()
    {
        CopyDirFiles(
            $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/sale_payment',
            $_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/include/sale_payment',
            true, true
        );
    }

    public function UnInstallFiles()
    {
        return DeleteDirFilesEx($_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/include/sale_payment/easypay');
    }
}